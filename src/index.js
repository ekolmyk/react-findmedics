import React from 'react';
import ReactDOM from 'react-dom';
// import { Provider } from 'react-redux';
// import { myStore } from './store';
// import { appConnector } from './components/app/connector';

import { Provider }  from 'react-redux';
// import { syncHistoryWithStore } from 'react-router-redux';
// import { applyMiddleware } from 'redux';

import { createStore, applyMiddleware } from 'redux';
import allRedusers from './reducers';
import App from './components/app/index';
import createHistory from 'history/createBrowserHistory';

// import { connect } from 'react-redux';

// window.store = myStore;
// const MyApp = appConnector(TodoApp);

import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';


const history = createHistory();// Create your chosen browser history

const middleware = routerMiddleware(history);// Create middleware to intercept and send navigation actions

// function testStore(state = [], action) { //=====> REDUCER --> reducers/TestStore.js added in the reducers/index.js for allRedusers
//     if (action.type === "ADD_TRACK") {
//         return [
//             ...state,
//             action.payload
//         ]
//     }
//     return state;
// }

// const store = createStore (allRedusers);
// const store = createStore (allRedusers, applyMiddleware(middleware), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()); // "Redux DevTools": https://chrome.google.com/webstore/search/ReduxDevTools
const store = createStore (allRedusers, composeWithDevTools(applyMiddleware(middleware, thunk))); // "Redux DevTools": https://chrome.google.com/webstore/search/ReduxDevTools

console.log(store.getState(allRedusers));           // Condition of our "store"
store.subscribe(() => {                             // Subscription to changes in our "store", using the method "subscribe"
    console.log('subscribe', store.getState());     // Type of modified to our "store"
})

store.dispatch({type: 'ADD_TRACK', payload: 'Test first script'});      // Changes №1 to our "store" (testStoreLive: Array(1))
store.dispatch({type: 'ADD_TRACK', payload: 'Enter second script'});    // Changes №2 to our "store" (testStoreLive: Array(2))

ReactDOM.render(
    <Provider store={ store }>
        <ConnectedRouter history={history}>
            <App />
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root')
);

export default function () {
    return [
        {
            "id": "i546",
            "dateIt": "today",
            "timeIt": "10:00"
        },
        {
            "id": "i547",
            "dateIt": "today",
            "timeIt": "10:30"
        },
        {
            "id": "i548",
            "dateIt": "today",
            "timeIt": "11:00"
        },
        {
            "id": "i549",
            "dateIt": "today",
            "timeIt": "11:30"
        },
        {
            "id": "i550",
            "dateIt": "today",
            "timeIt": "12:00"
        },
        {
            "id": "i551",
            "dateIt": "today",
            "timeIt": "12:30"
        },
        {
            "id": "i552",
            "dateIt": "today",
            "timeIt": "13:00"
        },
        {
            "id": "i553",
            "dateIt": "today",
            "timeIt": "13:30"
        },
        {
            "id": "i554",
            "dateIt": "today",
            "timeIt": "14:00"
        },
        {
            "id": "i555",
            "dateIt": "today",
            "timeIt": "16:50"
        },
        {
            "id": "i556",
            "dateIt": "today",
            "timeIt": "17:05"
        },
        {
            "id": "i557",
            "dateIt": "today",
            "timeIt": "17:30"
        },
        {
            "id": "i558",
            "dateIt": "today",
            "timeIt": "17:45"
        },
        {
            "id": "i559",
            "dateIt": "today",
            "timeIt": "18:10"
        },
        {
            "id": "i560",
            "dateIt": "today",
            "timeIt": "18:25"
        }
    ]
}

//==== EXEMPLE 1 ====//
// export default function (state = [], action) {
//     switch (action.type) {
//         case "ADD_TRACK":
//             return [
//                 ...state,
//                 action.payload
//             ];
//         break;
//         default:
//             return state;
//     }
// }

//==== EXEMPLE 2 ====//
export default function (state = [], action) {
    if (action.type === "ADD_TRACK") {
        return [
            ...state,
            action.payload
        ];
    }
    return state;
}


import React, { Component } from 'react';
// import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// import { selectForm } from "../../actions/actionContactForm";
import { reduxForm, Field } from 'redux-form';


import SelectCustom from '../custom-elements/SelectCustom';
import FieldCustom from '../custom-elements/FieldCustom';
import ButtonCustom from '../custom-elements/ButtonCustom';


const renderSelectField = ({ input, ...custom }) => (
    <SelectCustom
        {...input}
        valueSelected={input.value}
        onChange={(event, value) => input.onChange(value)}
        {...custom}
    />
)

class ContactForm extends Component {
    constructor(props){
        super(props)
        this.submitForm = this.submitForm.bind(this)
    }
    submitForm (event) { //SubmitForm
        event.preventDefault();
        console.log('on Submit Form!');
        alert(JSON.stringify(this.props.testValueForm.values));
    }
    render() {
        return(
            <div className="col s12 m5 l5 offset-l2 z-depth-1">
                <form
                    className="section"
                    onSubmit={this.submitForm}
                >
                    <FieldCustom
                        name="username"
                        hasLabel="true"
                        htmlFor="username"
                        label="Name *"
                        required="true"
                    />
                    <FieldCustom
                        name="useremail"
                        type="email"
                        hasLabel="true"
                        htmlFor="useremail"
                        label="Email *"
                        required="true"
                    />
                    <FieldCustom
                        name="phone"
                        activeLabel="active"
                        htmlFor="phone"
                        label="Mobile phone number"
                        placeholder="+125333677778"
                    />
                    <Field
                        name="select"
                        label="Subject"
                        defaultValue="Select options"
                        options="Option 1, Option 2, Option 3"
                        component={renderSelectField}
                    />
                    <FieldCustom
                        class="materialize-textarea"
                        component="textarea"
                        name="textarea"
                        hasLabel="true"
                        htmlFor="textarea"
                        label="Write here"
                    />
                    <div className="row">
                        <div className="input-field col s12 center">
                            <ButtonCustom
                                type="submit"
                                value="Submit"
                                text="Submit"
                                name="action"
                                className="waves-effect waves-light"
                            />
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}



function mapStateToProps (state) {
    return {
        // testLink: state.dataForm,
        testValueForm: state.form.contactForm
    };
}
//
// function matchDispatchToProps (dispatch) {
//     return bindActionCreators({selectForm2: selectForm}, dispatch)
// }
//
//
// export default connect(mapStateToProps, matchDispatchToProps)(ContactForm);

ContactForm = reduxForm({
    form: 'contactForm'
})(ContactForm);
// export default ContactForm;
export default connect(mapStateToProps)(ContactForm);
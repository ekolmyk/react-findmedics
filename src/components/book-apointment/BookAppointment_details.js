import React, { Component } from 'react';
import { connect } from 'react-redux';

import ButtonCustom from '../../components/custom-elements/ButtonCustom';


class DetailsTime extends Component {
    render() {
        if (!this.props.BookAppointmentTimeAlone) {
            return (
                <div className='modal modalbBox is-active'>
                    <div className="modal-content">
                        <div className='center'>Mark the parameters of interest!</div>
                    </div>
                    <div className="modal-footer">
                        <ButtonCustom
                            onClick={this.modalToggle}
                            text="Close"
                            name="action"
                            className="waves-effect waves-light"
                        />
                    </div>
                </div>
                )
        }
        return(
            <div className='modal modalbBox is-active'>
                <div className="modal-content">
                    <div className='center'>Select practice: {this.props.practice || 'nothing selected'}</div>
                    {/*<div className='center'>{this.props.location.city}</div>*/}
                    <div className='center'>Time of visit: {(this.props.BookAppointmentTimeAlone.dateIt + ' ' + this.props.BookAppointmentTimeAlone.timeIt)  || 'nothing selected'}</div>
                    <img width="100%"
                         height="368px"
                         style={{borderRadius: 3}}
                         src="https://source.unsplash.com/random"
                         alt="unsplash"/>
                </div>
                <div className="modal-footer">
                    <ButtonCustom
                        onClick={this.modalToggle}
                        text="Close"
                        name="action"
                        className="waves-effect waves-light"
                    />
                </div>
            </div>
        )
    }
}

function mapStateToProps (state) {
    return {
        BookAppointmentTimeAlone: state.dataBookAppointment_active
    };
}

export default connect(mapStateToProps)(DetailsTime);


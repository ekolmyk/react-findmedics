import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { selectTimeData } from '../../actions/BookAppointment_action';


class BookAppointment_TimeButton extends Component {
    constructor(props) {
        super(props)
        this.state = {
            minute: new Date().getMinutes(),
            hours: new Date().getHours()
        }
    }

    showListTimeToday() {
        return this.props.dataBookAppointmentToday.map((newSelectTimeDataToday) => {
            const timeNow = newSelectTimeDataToday.timeIt
            const timeZone = this.state.hours + ':' + this.state.minute
            if (timeNow >= timeZone) {
                return (
                    <div
                        key={newSelectTimeDataToday.id}
                        data-number={newSelectTimeDataToday.dateIt}
                        className="radioPad test"
                    >
                        <div>
                            <label>
                                <input
                                    className="radioPad-radio"
                                    type="radio"
                                    name="timeFree"
                                    id={newSelectTimeDataToday.id}
                                    value={newSelectTimeDataToday.dateIt + ' ' + newSelectTimeDataToday.timeIt}
                                    onChange={() => this.props.selectTimeData (newSelectTimeDataToday)}
                                />
                                <span className='radioPad-wrapper z-depth-1 waves-effect waves-red'>{newSelectTimeDataToday.timeIt}</span>
                            </label>
                        </div>
                    </div>
                )
            }
        })
    }
    showListTimeTomorrow() {
        return this.props.dataBookAppointmentTomorrow.map ((newSelectTimeDataTomorrow) => {
            return (
                <div
                    key={newSelectTimeDataTomorrow.id}
                    data-number={newSelectTimeDataTomorrow.dateIt}
                    className="radioPad test"
                >
                    <div>
                        <label>
                            <input
                                className="radioPad-radio"
                                type="radio"
                                name="timeFree"
                                id={newSelectTimeDataTomorrow.id}
                                value={newSelectTimeDataTomorrow.dateIt + ' ' + newSelectTimeDataTomorrow.timeIt}
                                onChange={() => this.props.selectTimeData (newSelectTimeDataTomorrow)}
                            />
                            <span className='radioPad-wrapper z-depth-1 waves-effect waves-red'>{newSelectTimeDataTomorrow.timeIt}</span>
                        </label>
                    </div>
                </div>
            )
        })
    }

    render() {
        const todayAvailable = this.showListTimeToday().filter(function (x) {
            return x !== undefined;
        });
        return (
            <div className="row">
                <div className="radioContainer nextAvailable">
                    <h5 className="center">
                        {(todayAvailable.length) ? 'Next available' : null}
                    </h5>
                    <div className="radioBox">
                        {todayAvailable.shift()}
                    </div>
                </div>
                <div className="radioContainer today">
                    <h5 className="center">{(todayAvailable.length) ? 'Today' : null}</h5>
                    <div className="radioBox">
                        {todayAvailable}
                    </div>
                </div>
                <div className="radioContainer tomorrow">
                    <h5 className="center">{this.showListTimeTomorrow().length ? 'Tomorrow' : null}</h5>
                    <div className="radioBox">
                        {this.showListTimeTomorrow()}
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps (state) {
    return {
        dataBookAppointmentToday: state.dataBookAppointmentToday,
        dataBookAppointmentTomorrow: state.dataBookAppointmentTomorrow
    };
}

function matchDispatchToProps (dispatch) {
    return bindActionCreators({selectTimeData: selectTimeData}, dispatch)
}


export default connect(mapStateToProps, matchDispatchToProps)(BookAppointment_TimeButton);

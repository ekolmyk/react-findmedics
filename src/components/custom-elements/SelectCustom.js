import React, { Component } from 'react';
import { Input, Row } from 'react-materialize';


class SelectCustom extends Component {
    render() {
        // Get all options from option prop
        const selectOptions = this.props.options.split(", ");

        // Generate list of options
        const selectOptionsList = selectOptions.map((selectOption, index) => {
            return <option key={index} value={selectOption}>{selectOption}</option>;
        });

        return (
            <Row>
                <Input
                    onChange={this.props.onChange || null}
                    s={12}
                    id={this.props.id || null}
                    name={this.props.name || null}
                    required={this.props.required || null}
                    type='select'
                    label={this.props.label || null}
                    defaultValue={this.props.defaultValue || ""}
                >
                    <option value={this.props.defaultValue} disabled>{this.props.defaultValue}</option>
                    {selectOptionsList}
                </Input>
            </Row>
        );
    }
}

export default SelectCustom;

import React, { Component } from 'react';
import { Row } from 'react-materialize';


class LabelCustom extends Component {
    render() {
        if (this.props.hasLabel === "true") {
            return (
                <label htmlFor={this.props.htmlFor}>{this.props.label}</label>
            );
        } else if (this.props.activeLabel === "active") {
            return (
                <label className="active" htmlFor={this.props.htmlFor}>{this.props.label}</label>
            );
        } else {
            return (
                <label></label>
            );
        }
    }
}

class InputCustom extends Component {
    render() {
        return (
            <Row>
                <div className="input-field col s12">
                    <input
                        onChange={this.props.onChange || null}
                        className={this.props.class || "validate"}
                        id={this.props.htmlFor}
                        max={this.props.max || null}
                        min={this.props.min || null}
                        name={this.props.name || null}
                        placeholder={this.props.placeholder || null}
                        required={this.props.required || null}
                        step={this.props.step || null}
                        type={this.props.type || "text"}
                        value={this.props.value || ""}
                    />
                    <LabelCustom
                        activeLabel={this.props.activeLabel}
                        hasLabel={this.props.hasLabel}
                        htmlFor={this.props.htmlFor}
                        label={this.props.label}
                    />
                </div>
            </Row>
        );
    }
}

export default InputCustom;
